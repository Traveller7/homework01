package area.and.perimeter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Square extends javax.swing.JFrame {

    public Square() {
        initComponents();
        this.setTitle("1514 - Area & Perimeter - Emme Chiquito...");
        this.setSize(393, 525);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        onlyNumbers(_length);
    }

    public void onlyNumbers(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                char character = evt.getKeyChar();
                if ((character < '0' || (character > '9')) && (character != KeyEvent.VK_BACK_SPACE) && (character != '.')) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (evt.getKeyChar() == '.' && _length.getText().contains(".")) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (_length.getText().length() > 9) {
                    evt.consume();
                    getToolkit().beep();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        _length = new javax.swing.JTextField();
        l1 = new javax.swing.JLabel();
        l2 = new javax.swing.JLabel();
        l3 = new javax.swing.JLabel();
        l4 = new javax.swing.JLabel();
        area = new javax.swing.JLabel();
        perimeter = new javax.swing.JLabel();
        ok = new javax.swing.JButton();
        text1 = new javax.swing.JLabel();
        oFigure = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        back = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        _length.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                _lengthActionPerformed(evt);
            }
        });
        getContentPane().add(_length);
        _length.setBounds(140, 170, 110, 24);
        getContentPane().add(l1);
        l1.setBounds(190, 240, 41, 16);
        getContentPane().add(l2);
        l2.setBounds(250, 300, 41, 16);
        getContentPane().add(l3);
        l3.setBounds(190, 350, 41, 16);
        getContentPane().add(l4);
        l4.setBounds(120, 300, 30, 16);

        area.setBackground(new java.awt.Color(255, 255, 255));
        area.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(area);
        area.setBounds(70, 380, 90, 20);

        perimeter.setBackground(new java.awt.Color(255, 255, 255));
        perimeter.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(perimeter);
        perimeter.setBounds(110, 400, 50, 20);

        ok.setBorderPainted(false);
        ok.setContentAreaFilled(false);
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        getContentPane().add(ok);
        ok.setBounds(140, 200, 110, 30);

        text1.setBackground(new java.awt.Color(255, 255, 255));
        text1.setText("Length of b:");
        getContentPane().add(text1);
        text1.setBounds(140, 150, 110, 16);

        oFigure.setForeground(new java.awt.Color(255, 255, 255));
        oFigure.setText("Choose Other Figure");
        oFigure.setBorderPainted(false);
        oFigure.setContentAreaFilled(false);
        oFigure.setFocusable(false);
        oFigure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oFigureActionPerformed(evt);
            }
        });
        getContentPane().add(oFigure);
        oFigure.setBounds(160, 432, 150, 30);

        exit.setForeground(new java.awt.Color(255, 255, 255));
        exit.setText("Exit");
        exit.setBorderPainted(false);
        exit.setContentAreaFilled(false);
        exit.setFocusable(false);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit);
        exit.setBounds(320, 432, 60, 30);

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Backgrouds/Square.jpg"))); // NOI18N
        getContentPane().add(back);
        back.setBounds(0, 0, 393, 500);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        ExitOrContinue s = new ExitOrContinue();
        s.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void _lengthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__lengthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event__lengthActionPerformed

    private void oFigureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oFigureActionPerformed
        this.setVisible(false);
        AskForFigures w1 = new AskForFigures();
        w1.setVisible(true);
    }//GEN-LAST:event_oFigureActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        if (_length.getText().equals("") || _length.getText().equals("0") || _length.getText().equals(".")) {
            JOptionPane.showMessageDialog(null, "You have to enter a valid value\nof lenght in (b)", "Enter Length", JOptionPane.ERROR_MESSAGE);
            _length.setText("");
            l1.setText("");
            l2.setText("");
            l3.setText("");
            l4.setText("");
            getToolkit().beep();
        } else {
            double cero = Double.parseDouble(_length.getText());
            if (cero < .000000001) {
                JOptionPane.showMessageDialog(null, "You have to enter a valid value\nof length in (b)", "Enter Length", JOptionPane.ERROR_MESSAGE);
                _length.setText("");
                l1.setText("");
                l2.setText("");
                l3.setText("");
                l4.setText("");
                getToolkit().beep();
            } else {
                double length = Double.parseDouble(_length.getText());
                double perimeterS = length * 4;
                double areaS = length * length;
                area.setText(String.valueOf(areaS));
                perimeter.setText(String.valueOf(perimeterS));
                l1.setText(String.valueOf(length));
                l2.setText(String.valueOf(length));
                l3.setText(String.valueOf(length));
                l4.setText(String.valueOf(length));
            }
        }
    }//GEN-LAST:event_okActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField _length;
    private javax.swing.JLabel area;
    private javax.swing.JLabel back;
    private javax.swing.JButton exit;
    private javax.swing.JLabel l1;
    private javax.swing.JLabel l2;
    private javax.swing.JLabel l3;
    private javax.swing.JLabel l4;
    private javax.swing.JButton oFigure;
    private javax.swing.JButton ok;
    private javax.swing.JLabel perimeter;
    private javax.swing.JLabel text1;
    // End of variables declaration//GEN-END:variables

}
