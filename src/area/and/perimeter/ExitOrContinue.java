package area.and.perimeter;

import static java.lang.System.exit;

public class ExitOrContinue extends javax.swing.JFrame {

    public ExitOrContinue() {
        initComponents();
        this.setTitle("1514 - Area & Perimeter - Emmanuel Chiquito Reséndiz...");
        this.setSize(260,100);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        question = new javax.swing.JLabel();
        exit1 = new javax.swing.JButton();
        goBack = new javax.swing.JButton();
        back = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        question.setText("Do you want go out of this program?");
        getContentPane().add(question);
        question.setBounds(28, 6, 204, 16);

        exit1.setForeground(new java.awt.Color(255, 255, 255));
        exit1.setText("Exit");
        exit1.setBorderPainted(false);
        exit1.setContentAreaFilled(false);
        exit1.setFocusable(false);
        exit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exit1ActionPerformed(evt);
            }
        });
        getContentPane().add(exit1);
        exit1.setBounds(144, 28, 60, 26);

        goBack.setForeground(new java.awt.Color(255, 255, 255));
        goBack.setText("Go Back");
        goBack.setBorderPainted(false);
        goBack.setContentAreaFilled(false);
        goBack.setFocusable(false);
        goBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goBackActionPerformed(evt);
            }
        });
        getContentPane().add(goBack);
        goBack.setBounds(44, 28, 90, 25);

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Backgrouds/exit.jpg"))); // NOI18N
        getContentPane().add(back);
        back.setBounds(0, 0, 260, 110);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void goBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goBackActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_goBackActionPerformed

    private void exit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exit1ActionPerformed
        exit(0);
    }//GEN-LAST:event_exit1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel back;
    private javax.swing.JButton exit1;
    private javax.swing.JButton goBack;
    private javax.swing.JLabel question;
    // End of variables declaration//GEN-END:variables
}
