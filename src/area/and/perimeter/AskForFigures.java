package area.and.perimeter;

public class AskForFigures extends javax.swing.JFrame {

    public AskForFigures() {
        initComponents();
        this.setTitle("1514 - Area & Perimeter - Emme Chiquito...");
        this.setSize(393,525);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        rectangle = new javax.swing.JButton();
        triangle = new javax.swing.JButton();
        square = new javax.swing.JButton();
        circle = new javax.swing.JButton();
        polygon = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        back = new javax.swing.JLabel();

        jMenuItem1.setText("jMenuItem1");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        rectangle.setBorderPainted(false);
        rectangle.setContentAreaFilled(false);
        rectangle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rectangleActionPerformed(evt);
            }
        });
        getContentPane().add(rectangle);
        rectangle.setBounds(150, 310, 200, 80);

        triangle.setBorderPainted(false);
        triangle.setContentAreaFilled(false);
        triangle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                triangleActionPerformed(evt);
            }
        });
        getContentPane().add(triangle);
        triangle.setBounds(50, 310, 80, 80);

        square.setBorderPainted(false);
        square.setContentAreaFilled(false);
        square.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                squareActionPerformed(evt);
            }
        });
        getContentPane().add(square);
        square.setBounds(280, 190, 80, 80);

        circle.setBorderPainted(false);
        circle.setContentAreaFilled(false);
        circle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                circleActionPerformed(evt);
            }
        });
        getContentPane().add(circle);
        circle.setBounds(160, 190, 80, 80);

        polygon.setBorderPainted(false);
        polygon.setContentAreaFilled(false);
        polygon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                polygonActionPerformed(evt);
            }
        });
        getContentPane().add(polygon);
        polygon.setBounds(40, 190, 80, 80);

        exit.setForeground(new java.awt.Color(255, 255, 255));
        exit.setText("Exit");
        exit.setBorderPainted(false);
        exit.setContentAreaFilled(false);
        exit.setFocusable(false);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit);
        exit.setBounds(320, 432, 60, 30);

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Backgrouds/1.jpg"))); // NOI18N
        getContentPane().add(back);
        back.setBounds(0, 0, 393, 500);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        ExitOrContinue s = new ExitOrContinue();
        s.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void polygonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_polygonActionPerformed
        Polygon p1 = new Polygon();
        p1.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_polygonActionPerformed

    private void circleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_circleActionPerformed
        Circle c1 = new Circle();
        c1.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_circleActionPerformed

    private void squareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_squareActionPerformed
        Square f3 = new Square();
        f3.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_squareActionPerformed

    private void triangleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_triangleActionPerformed
        Triangle t1 = new Triangle();
        t1.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_triangleActionPerformed

    private void rectangleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rectangleActionPerformed
        Rectangle r1 = new Rectangle();
        r1.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_rectangleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel back;
    private javax.swing.JButton circle;
    private javax.swing.JButton exit;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JButton polygon;
    private javax.swing.JButton rectangle;
    private javax.swing.JButton square;
    private javax.swing.JButton triangle;
    // End of variables declaration//GEN-END:variables

}