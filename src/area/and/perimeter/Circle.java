package area.and.perimeter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Circle extends javax.swing.JFrame {

    public Circle() {
        initComponents();
        this.setTitle("1514 - Area & Perimeter - Emme Chiquito...");
        this.setSize(393, 525);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        onlyNumbers(_radio);
    }

    public void onlyNumbers(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                char character = evt.getKeyChar();
                if ((character < '0' || (character > '9')) && (character != KeyEvent.VK_BACK_SPACE) && (character != '.')) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (evt.getKeyChar() == '.' && _radio.getText().contains(".")) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (_radio.getText().length() > 9) {
                    evt.consume();
                    getToolkit().beep();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        _radio = new javax.swing.JTextField();
        r = new javax.swing.JLabel();
        area = new javax.swing.JLabel();
        perimeter = new javax.swing.JLabel();
        ok = new javax.swing.JButton();
        text1 = new javax.swing.JLabel();
        oFigure = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        back = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        _radio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                _radioActionPerformed(evt);
            }
        });
        getContentPane().add(_radio);
        _radio.setBounds(140, 170, 110, 24);
        getContentPane().add(r);
        r.setBounds(160, 280, 50, 16);

        area.setBackground(new java.awt.Color(255, 255, 255));
        area.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(area);
        area.setBounds(70, 380, 90, 20);

        perimeter.setBackground(new java.awt.Color(255, 255, 255));
        perimeter.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(perimeter);
        perimeter.setBounds(110, 400, 50, 20);

        ok.setBorderPainted(false);
        ok.setContentAreaFilled(false);
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        getContentPane().add(ok);
        ok.setBounds(140, 200, 110, 30);

        text1.setBackground(new java.awt.Color(255, 255, 255));
        text1.setText("Length of r:");
        getContentPane().add(text1);
        text1.setBounds(140, 150, 110, 16);

        oFigure.setForeground(new java.awt.Color(255, 255, 255));
        oFigure.setText("Choose Other Figure");
        oFigure.setBorderPainted(false);
        oFigure.setContentAreaFilled(false);
        oFigure.setFocusable(false);
        oFigure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oFigureActionPerformed(evt);
            }
        });
        getContentPane().add(oFigure);
        oFigure.setBounds(160, 432, 150, 30);

        exit.setForeground(new java.awt.Color(255, 255, 255));
        exit.setText("Exit");
        exit.setBorderPainted(false);
        exit.setContentAreaFilled(false);
        exit.setFocusable(false);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit);
        exit.setBounds(320, 432, 60, 30);

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Backgrouds/circle.jpg"))); // NOI18N
        getContentPane().add(back);
        back.setBounds(0, 0, 393, 500);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        ExitOrContinue s = new ExitOrContinue();
        s.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void _radioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__radioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event__radioActionPerformed

    private void oFigureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oFigureActionPerformed
        this.setVisible(false);
        AskForFigures w1 = new AskForFigures();
        w1.setVisible(true);
    }//GEN-LAST:event_oFigureActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        if (_radio.getText().equals("") || _radio.getText().equals("0") || _radio.getText().equals(".")) {
            JOptionPane.showMessageDialog(null, "You have to enter a valid value\nof length in (r)", "Enter Radio", JOptionPane.ERROR_MESSAGE);
            _radio.setText("");
            r.setText("");
            getToolkit().beep();
        } else {
            double cero = Double.parseDouble(_radio.getText());
            if (cero < .000000001) {
                JOptionPane.showMessageDialog(null, "You have to enter a valid value\nof length in (r)", "Enter Radio", JOptionPane.ERROR_MESSAGE);
                _radio.setText("");
                r.setText("");
                getToolkit().beep();
            } else {
                double radio = Double.parseDouble(_radio.getText());
                double perimeterS = 2 * 3.1416 * radio;
                double areaS = (3.1416 * Math.pow(radio, 2));
                area.setText(String.valueOf(areaS));
                perimeter.setText(String.valueOf(perimeterS));
                r.setText(String.valueOf(radio));
            }
        }
    }//GEN-LAST:event_okActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField _radio;
    private javax.swing.JLabel area;
    private javax.swing.JLabel back;
    private javax.swing.JButton exit;
    private javax.swing.JButton oFigure;
    private javax.swing.JButton ok;
    private javax.swing.JLabel perimeter;
    private javax.swing.JLabel r;
    private javax.swing.JLabel text1;
    // End of variables declaration//GEN-END:variables

}
