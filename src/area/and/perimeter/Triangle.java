package area.and.perimeter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Triangle extends javax.swing.JFrame {

    public Triangle() {
        initComponents();
        this.setTitle("1514 - Area & Perimeter - Emme Chiquito...");
        this.setSize(393, 525);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        onlyNumbers_a(_a);
        onlyNumbers_b(_b);
        onlyNumbers_c(_c);
    }

    public void onlyNumbers_a(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                char character = evt.getKeyChar();
                if ((character < '0' || (character > '9')) && (character != KeyEvent.VK_BACK_SPACE) && (character != '.')) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (evt.getKeyChar() == '.' && _a.getText().contains(".")) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (_a.getText().length() > 9) {
                    evt.consume();
                    getToolkit().beep();
                }
            }
        });
    }

    public void onlyNumbers_b(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                char character = evt.getKeyChar();
                if ((character < '0' || (character > '9')) && (character != KeyEvent.VK_BACK_SPACE) && (character != '.')) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (evt.getKeyChar() == '.' && _b.getText().contains(".")) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (_b.getText().length() > 9) {
                    evt.consume();
                    getToolkit().beep();
                }
            }
        });
    }

    public void onlyNumbers_c(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                char character = evt.getKeyChar();
                if ((character < '0' || (character > '9')) && (character != KeyEvent.VK_BACK_SPACE) && (character != '.')) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (evt.getKeyChar() == '.' && _c.getText().contains(".")) {
                    evt.consume();
                    getToolkit().beep();
                }
                if (_c.getText().length() > 9) {
                    evt.consume();
                    getToolkit().beep();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        _c = new javax.swing.JTextField();
        _b = new javax.swing.JTextField();
        text2 = new javax.swing.JLabel();
        text3 = new javax.swing.JLabel();
        _a = new javax.swing.JTextField();
        text1 = new javax.swing.JLabel();
        typeOfTriangle = new javax.swing.JLabel();
        as = new javax.swing.JLabel();
        bs = new javax.swing.JLabel();
        cs = new javax.swing.JLabel();
        area = new javax.swing.JLabel();
        perimeter = new javax.swing.JLabel();
        ok = new javax.swing.JButton();
        oFigure = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        back = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        _c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                _cActionPerformed(evt);
            }
        });
        getContentPane().add(_c);
        _c.setBounds(260, 170, 110, 24);

        _b.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                _bActionPerformed(evt);
            }
        });
        getContentPane().add(_b);
        _b.setBounds(140, 170, 110, 24);

        text2.setBackground(new java.awt.Color(255, 255, 255));
        text2.setText("Length of b:");
        getContentPane().add(text2);
        text2.setBounds(140, 150, 110, 16);

        text3.setBackground(new java.awt.Color(255, 255, 255));
        text3.setText("Length of c:");
        getContentPane().add(text3);
        text3.setBounds(260, 150, 110, 16);

        _a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                _aActionPerformed(evt);
            }
        });
        getContentPane().add(_a);
        _a.setBounds(20, 170, 110, 24);

        text1.setBackground(new java.awt.Color(255, 255, 255));
        text1.setText("Length of a:");
        getContentPane().add(text1);
        text1.setBounds(20, 150, 110, 16);

        typeOfTriangle.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(typeOfTriangle);
        typeOfTriangle.setBounds(180, 404, 200, 20);
        getContentPane().add(as);
        as.setBounds(190, 350, 41, 16);
        getContentPane().add(bs);
        bs.setBounds(230, 290, 41, 16);
        getContentPane().add(cs);
        cs.setBounds(140, 290, 30, 16);

        area.setBackground(new java.awt.Color(255, 255, 255));
        area.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(area);
        area.setBounds(70, 380, 90, 20);

        perimeter.setBackground(new java.awt.Color(255, 255, 255));
        perimeter.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(perimeter);
        perimeter.setBounds(110, 400, 50, 20);

        ok.setBorderPainted(false);
        ok.setContentAreaFilled(false);
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        getContentPane().add(ok);
        ok.setBounds(140, 200, 110, 30);

        oFigure.setForeground(new java.awt.Color(255, 255, 255));
        oFigure.setText("Choose Other Figure");
        oFigure.setBorderPainted(false);
        oFigure.setContentAreaFilled(false);
        oFigure.setFocusable(false);
        oFigure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oFigureActionPerformed(evt);
            }
        });
        getContentPane().add(oFigure);
        oFigure.setBounds(160, 432, 150, 30);

        exit.setForeground(new java.awt.Color(255, 255, 255));
        exit.setText("Exit");
        exit.setBorderPainted(false);
        exit.setContentAreaFilled(false);
        exit.setFocusable(false);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit);
        exit.setBounds(320, 432, 60, 30);

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Backgrouds/triangle.jpg"))); // NOI18N
        getContentPane().add(back);
        back.setBounds(0, 0, 393, 500);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        ExitOrContinue s = new ExitOrContinue();
        s.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void oFigureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oFigureActionPerformed
        this.setVisible(false);
        AskForFigures w1 = new AskForFigures();
        w1.setVisible(true);
    }//GEN-LAST:event_oFigureActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        if (_a.getText().equals("") || _b.getText().equals("") || _c.getText().equals("") || _a.getText().equals("0") || _b.getText().equals("0") || _c.getText().equals("0") || _a.getText().equals(".") || _b.getText().equals(".") || _c.getText().equals(".")) {
            JOptionPane.showMessageDialog(null, "You have to enter valid values\nof length in a, b and c", "Enter lenghts", JOptionPane.ERROR_MESSAGE);
            _a.setText("");
            _b.setText("");
            _c.setText("");
            as.setText("");
            bs.setText("");
            cs.setText("");
            getToolkit().beep();
        } else {
            double cero = Double.parseDouble(_a.getText());
            double cero2 = Double.parseDouble(_b.getText());
            double cero3 = Double.parseDouble(_c.getText());
            if (cero < .000000001 || cero2 < .000000001 || cero3 < .000000001) {
                JOptionPane.showMessageDialog(null, "You have to enter valid values\nof length in (a), (b) and (c)", "Enter lenghts", JOptionPane.ERROR_MESSAGE);
                _a.setText("");
                _b.setText("");
                _c.setText("");
                as.setText("");
                bs.setText("");
                cs.setText("");
                getToolkit().beep();
            } else {
                double a = Double.parseDouble(_a.getText());
                double b = Double.parseDouble(_b.getText());
                double c = Double.parseDouble(_c.getText());
                if (a < b + c && b < a + c && c < a + b) {
                    double perimeterS = a + b + c;
                    double semiPerimeter = (a + b + c) / 2;
                    double areaS = Math.sqrt(semiPerimeter * (semiPerimeter - a) * (semiPerimeter - b) * (semiPerimeter - c));
                    area.setText(String.valueOf(areaS));
                    perimeter.setText(String.valueOf(perimeterS));
                    as.setText(String.valueOf(a));
                    bs.setText(String.valueOf(b));
                    cs.setText(String.valueOf(c));
                    if (a == b && a == b) {
                        typeOfTriangle.setText("Note: It's an equilateral triangle...");
                    }
                    if (b == c && a != b) {
                        typeOfTriangle.setText("Note: It's an isosceles triangle...");
                    }
                    if (b != c) {
                        typeOfTriangle.setText("Note: It's an scalene triangle...");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "You can't make a triangle with these values\nEnter different values, please...", "Error with values", JOptionPane.ERROR_MESSAGE);
                    _a.setText("");
                    _b.setText("");
                    _c.setText("");
                    as.setText("");
                    bs.setText("");
                    cs.setText("");
                    getToolkit().beep();
                }
            }
        }
    }//GEN-LAST:event_okActionPerformed

    private void _cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event__cActionPerformed

    private void _bActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__bActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event__bActionPerformed

    private void _aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__aActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event__aActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField _a;
    private javax.swing.JTextField _b;
    private javax.swing.JTextField _c;
    private javax.swing.JLabel area;
    private javax.swing.JLabel as;
    private javax.swing.JLabel back;
    private javax.swing.JLabel bs;
    private javax.swing.JLabel cs;
    private javax.swing.JButton exit;
    private javax.swing.JButton oFigure;
    private javax.swing.JButton ok;
    private javax.swing.JLabel perimeter;
    private javax.swing.JLabel text1;
    private javax.swing.JLabel text2;
    private javax.swing.JLabel text3;
    private javax.swing.JLabel typeOfTriangle;
    // End of variables declaration//GEN-END:variables

}
